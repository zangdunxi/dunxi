package com.example.springday01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDay01Application {

    public static void main(String[] args) {
        System.out.println("山东省大学生计算机学会");
        SpringApplication.run(SpringDay01Application.class, args);
    }

}
